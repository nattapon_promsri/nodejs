
/// <reference types ="Cypress" />
describe('go to ICST', () => {

    it('To Do (add)', () => {
        cy.visit('localhost')
        cy.get('[href="/task/add"]').click()
        cy.get('#summary').type('yummy')
        cy.get('#detail').type('That do you yummy yummy yum that yummy yummy, That do you yummy yummy yum that yummy yummy ,say the word on myway yeah babe yeah babe yeah babe anynight anyday')
        cy.get('#priority').select('Normal')
        cy.get('#duedate').type('2020-01-15')
        cy.get('.btn').click()

        cy.get('[href="/task/add"]').click()
        cy.get('#summary').type('red && grenn')
        cy.get('#detail').type('ไอแดงมันเป็นนักสู้ที่คล่องแคล่วปราดเปรียวและไอเขียวมันเป็นนักซิ่งที่มีมอไซด์ที่วิ่งแซงทุกคัน ไปปปป')
        cy.get('#priority').select('Low')
        cy.get('#duedate').type('2019-01-15')
        cy.get('.btn').click()
    })


    it('To Do (edit)', () => {
        cy.wait(2000)

        cy.get(':nth-child(2) > .card-footer > .row > :nth-child(2) > .nav > :nth-child(1) > .nav-link > .fas').click()

        cy.get('#summary').clear('')
        cy.get('#summary').type('test1111111')

        cy.get('#detail').clear('')
        cy.get('#detail').type('test To Do')

        cy.get('#priority').select('High')

        cy.get('#duedate').type('2021-11-12')
        cy.get('.btn').click()
    })


    it('To Do (delete)', () => {
        cy.wait(2000)

        cy.get('[cl=""] > .nav-link').click()
        cy.get('.btn-primary').click()

        cy.get('[cl=""] > .nav-link').click()
        cy.get('.btn-danger').click()
    })



    it('To Do (mark)', () => {
        cy.wait(2000)

        cy.get('[cl=""] > .nav-link').click()
        cy.get('.btn-primary').click()

        cy.get('[cl=""] > .nav-link').click()
        cy.get('.btn-danger').click()
    })



})